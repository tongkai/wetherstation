package com.chenfeng.wetherstation.base;

/**
 * Created by GEC-IOS100 on 15/11/28.
 */
public class BaseJsonModel {
    protected int status;
    protected String url;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "BaseJsonModel{" +
                "status=" + status +
                ", url='" + url + '\'' +
                '}';
    }
}
