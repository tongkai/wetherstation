package com.chenfeng.wetherstation.base;

import android.app.Application;
import android.widget.BaseAdapter;

import com.chenfeng.wetherstation.model.InfoJsonModel;
import com.chenfeng.wetherstation.utils.ConstantsUtils;
import com.chenfeng.wetherstation.utils.okhttputils.OkHttpClientManager;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

/**
 * Created by GEC-IOS100 on 15/11/27.
 */
public class BaseApplication extends Application{
    private static BaseApplication app;
    private static InfoJsonModel user;//保存登录成功后返回的信息
    public static BaseApplication getInstance(){
        return app;
    }
    public static InfoJsonModel getUserInstance(){
        if (user == null){
            synchronized (BaseApplication.class){
                if (user == null){
                    user = new InfoJsonModel();
                }
            }
        }
        return user;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        initOkHttp();
    }

    private void initOkHttp(){
        OkHttpClient client = OkHttpClientManager.getInstance().getOkHttpClient();
        client.setConnectTimeout(ConstantsUtils.CONNECT_TIME_OUT, TimeUnit.MILLISECONDS);
    }
}
