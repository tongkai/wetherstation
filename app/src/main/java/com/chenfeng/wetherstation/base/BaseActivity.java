package com.chenfeng.wetherstation.base;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * 暂定
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void initView(){}

    protected void clickEvent(){}
}
