package com.chenfeng.wetherstation.model;

import java.util.List;

/**
 * Created by GEC-IOS100 on 15/11/21.
 */
public class Province {
    private String id;
    private String name;
    private List<City> cities;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }
}
