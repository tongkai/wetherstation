package com.chenfeng.wetherstation.model;

import com.chenfeng.wetherstation.base.BaseJsonModel;

/**
 * Created by GEC-IOS100 on 15/11/28.
 */
public class StringJsonModel extends BaseJsonModel{

    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "StringJsonModel{" +
                "info='" + info + '\'' +
                "} " + super.toString();
    }
}
