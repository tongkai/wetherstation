package com.chenfeng.wetherstation.model;

import com.chenfeng.wetherstation.base.BaseJsonModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by GEC-IOS100 on 15/12/8.
 */
public class InfoJsonModel extends BaseJsonModel{

    private Info info;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public static class Info{
        private String id;
        private String aid;
        private String recuid;
        private List<String> dstritid;// 不知道是什么类型
        private String username;
        private String realname;
        private String gender;
        private String thumb;
        private String education;
        @SerializedName("super")
        private Boolean isSuper;
        @SerializedName("extends")
        private Extend extend;
        private String pid;
        private String arealname;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAid() {
            return aid;
        }

        public void setAid(String aid) {
            this.aid = aid;
        }

        public String getRecuid() {
            return recuid;
        }

        public void setRecuid(String recuid) {
            this.recuid = recuid;
        }

        public List<String> getDstritid() {
            return dstritid;
        }

        public void setDstritid(List<String> dstritid) {
            this.dstritid = dstritid;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public String getEducation() {
            return education;
        }

        public void setEducation(String education) {
            this.education = education;
        }

        public Boolean getIsSuper() {
            return isSuper;
        }

        public void setIsSuper(Boolean isSuper) {
            this.isSuper = isSuper;
        }

        public Extend getExtend() {
            return extend;
        }

        public void setExtend(Extend extend) {
            this.extend = extend;
        }

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getArealname() {
            return arealname;
        }

        public void setArealname(String arealname) {
            this.arealname = arealname;
        }

        @Override
        public String toString() {
            return "Info{" +
                    "id='" + id + '\'' +
                    ", aid='" + aid + '\'' +
                    ", recuid='" + recuid + '\'' +
                    ", dstritid=" + dstritid +
                    ", username='" + username + '\'' +
                    ", realname='" + realname + '\'' +
                    ", gender='" + gender + '\'' +
                    ", thumb='" + thumb + '\'' +
                    ", education='" + education + '\'' +
                    ", isSuper=" + isSuper +
                    ", extend=" + extend +
                    ", pid='" + pid + '\'' +
                    ", arealname='" + arealname + '\'' +
                    '}';
        }
    }
    public static class Extend{
        private String address;
        private int reqtime;//又是什么类型
        private String status;
        private String salt;
        private String email;
        private String qq;
        private String phone;
        private Module module;
        private String weavip;
        private String district;
        private int logins;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getReqtime() {
            return reqtime;
        }

        public void setReqtime(int reqtime) {
            this.reqtime = reqtime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getQq() {
            return qq;
        }

        public void setQq(String qq) {
            this.qq = qq;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Module getModule() {
            return module;
        }

        public void setModule(Module module) {
            this.module = module;
        }

        public String getWeavip() {
            return weavip;
        }

        public void setWeavip(String weavip) {
            this.weavip = weavip;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public int getLogins() {
            return logins;
        }

        public void setLogins(int logins) {
            this.logins = logins;
        }

        @Override
        public String toString() {
            return "Extend{" +
                    "address='" + address + '\'' +
                    ", reqtime=" + reqtime +
                    ", status='" + status + '\'' +
                    ", salt='" + salt + '\'' +
                    ", email='" + email + '\'' +
                    ", qq='" + qq + '\'' +
                    ", phone='" + phone + '\'' +
                    ", module=" + module +
                    ", weavip='" + weavip + '\'' +
                    ", district='" + district + '\'' +
                    ", logins=" + logins +
                    '}';
        }
    }

    public static class Module{
        private String w;

        public String getW() {
            return w;
        }

        public void setW(String w) {
            this.w = w;
        }

        @Override
        public String toString() {
            return "Module{" +
                    "w='" + w + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "InfoJsonModel{" +
                "info=" + info +
                "} " + super.toString();
    }


}
