package com.chenfeng.wetherstation.model;

import android.widget.ImageView;
import android.widget.TextView;

import com.chenfeng.wetherstation.adapter.MeSettingAdapter;

/**
 * Created by GEC-IOS100 on 15/11/26.
 */
public class MeSettingItem {
    private int resId;
    private String text;

    public MeSettingItem(int resId,String text){
        this.resId = resId;
        this.text = text;
    }
    public MeSettingItem(){}

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
