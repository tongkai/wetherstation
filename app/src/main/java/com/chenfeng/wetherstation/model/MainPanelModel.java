package com.chenfeng.wetherstation.model;

/**
 * Created by GEC-IOS100 on 15/12/1.
 */
public class MainPanelModel {
    private int resId;
    private String title;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
