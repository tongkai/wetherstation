package com.chenfeng.wetherstation.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.EditText;

import com.chenfeng.wetherstation.R;

/**
 * 用来实现切换密码的显示和隐藏
 * Created by GEC-IOS100 on 15/11/26.
 */
public class TKPasswordEdittext extends EditText{

    private Drawable mViewDrawable;

    private boolean isHiden = true;

    public TKPasswordEdittext(Context context) {
       this(context,null);
    }

    public TKPasswordEdittext(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.editTextStyle);

    }

    public TKPasswordEdittext(Context context, AttributeSet attrs,int defStyle) {

       super(context,attrs,defStyle);
        init();
    }

    private void init(){
        mViewDrawable = getCompoundDrawables()[2];
        if (mViewDrawable == null){
            mViewDrawable = getResources().getDrawable(R.mipmap.ic_un_view);
        }
        mViewDrawable.setBounds(0, 0, mViewDrawable.getIntrinsicWidth(), mViewDrawable.getIntrinsicHeight());
    }

   private void changeRightDrawable(){
       if (isHiden){
           mViewDrawable = getResources().getDrawable(R.mipmap.ic_un_view);
       }else {
           mViewDrawable = getResources().getDrawable(R.mipmap.ic_on_view);
       }
       mViewDrawable.setBounds(0, 0, mViewDrawable.getIntrinsicWidth(), mViewDrawable.getIntrinsicHeight());
       setCompoundDrawables(getCompoundDrawables()[0],
               getCompoundDrawables()[1], mViewDrawable, getCompoundDrawables()[3]);
   }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP){
            if (mViewDrawable != null){
                if (event.getX() > getWidth() - getTotalPaddingRight() && event.getX() < getWidth() - getPaddingRight()){
                    Log.d("tag","click drawable"+isHiden);
                    if(!isHiden){
                        //隐藏
                        setTransformationMethod(PasswordTransformationMethod.getInstance());
                        isHiden = true;
                    }else {
                        //显示
                        setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        isHiden = false;
                    }
                    changeRightDrawable();
                    //切换后要重绘
                    invalidate();
                    //切换后将EditText光标置于末尾
                    CharSequence charSequence = getText();
                    if (charSequence instanceof Spannable) {
                        Spannable spanText = (Spannable) charSequence;
                        Selection.setSelection(spanText, charSequence.length());
                    }
                }
            }
        }
        return super.onTouchEvent(event);
    }
}
