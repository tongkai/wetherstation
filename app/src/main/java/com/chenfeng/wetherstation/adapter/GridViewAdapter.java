package com.chenfeng.wetherstation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chenfeng.wetherstation.R;
import com.chenfeng.wetherstation.model.MainPanelModel;

import java.util.List;

/**
 * Created by GEC-IOS100 on 15/12/5.
 */
public class GridViewAdapter extends BaseAdapter{
    private Context context;
    private List<MainPanelModel> list;

    public GridViewAdapter(Context context,List<MainPanelModel> list){
        this.context = context;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_main_menu_item,parent,false);
            holder.img = (ImageView) convertView.findViewById(R.id.main_menu_icon);
            holder.title = (TextView) convertView.findViewById(R.id.main_menu_title);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.img.setImageResource(list.get(position).getResId());
        holder.title.setText(list.get(position).getTitle());
        return convertView;
    }

    static class ViewHolder{
        ImageView img;
        TextView title;
    }
}
