package com.chenfeng.wetherstation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chenfeng.wetherstation.R;
import com.chenfeng.wetherstation.model.MeSettingItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GEC-IOS100 on 15/11/26.
 */
public class MeSettingAdapter extends BaseAdapter{
    private Context context;
    private List<MeSettingItem> list;

    public MeSettingAdapter(Context context){
        this.context = context;
        initDatas();
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (holder == null){

            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.me_setting_item,null);
            holder.imageView = (ImageView) convertView.findViewById(R.id.img_me_setting_item);
            holder.textView = (TextView) convertView.findViewById(R.id.tv_me_setting_item);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        MeSettingItem item = (MeSettingItem) getItem(position);
        if (item != null){
            holder.imageView.setImageResource(item.getResId());
            holder.textView.setText(item.getText());
        }

        return convertView;
    }

    private void initDatas(){
        list = new ArrayList<MeSettingItem>();
        MeSettingItem item = new MeSettingItem(R.mipmap.ic_change_data,context.getResources().getString(R.string.change_data));
        list.add(item);
        item = new MeSettingItem(R.mipmap.ic_change_password,context.getResources().getString(R.string.change_password));
        list.add(item);
        item = new MeSettingItem(R.mipmap.ic_cancel_bind,context.getResources().getString(R.string.cancel_bind));
        list.add(item);
    }

    private static class ViewHolder{
        ImageView imageView;
        TextView textView;
    }
}

