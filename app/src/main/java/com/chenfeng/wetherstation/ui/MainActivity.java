package com.chenfeng.wetherstation.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chenfeng.wetherstation.R;
import com.chenfeng.wetherstation.adapter.GridViewAdapter;
import com.chenfeng.wetherstation.base.BaseApplication;
import com.chenfeng.wetherstation.libcore.io.DiskLruCache;
import com.chenfeng.wetherstation.model.MainPanelModel;
import com.chenfeng.wetherstation.utils.DiskLruCacheUtils;
import com.chenfeng.wetherstation.utils.ToptabUtils;
import com.chenfeng.wetherstation.utils.okhttputils.callback.ResultCallback;

import com.chenfeng.wetherstation.utils.okhttputils.request.OkHttpRequest;
import com.chenfeng.wetherstation.widget.CircleImageView;
import com.squareup.okhttp.Request;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final static int COLNUM = 3;
    private ImageView topLeftMenu;
    private TextView back;
    private TextView topTabTitle;

    private List<MainPanelModel> list;

    private GridView mGridView;
    private CircleImageView headIcon;
    private TextView nickNameTextView;

    private DiskLruCache mCache;

    private int[] imgResidArray = {R.mipmap.ic_main_vegetable,R.mipmap.ic_main_watered,R.mipmap.ic_main_environment,
            R.mipmap.ic_main_recharge,R.mipmap.ic_main_changedata,R.mipmap.ic_main_changepsw,
            R.mipmap.ic_main_use_explain,R.mipmap.ic_main_cancelbind,R.mipmap.ic_main_install_sure};
    private int[] textResidArray = {R.string.vegetables,R.string.water_auto,R.string.enviroment_detect,
            R.string.recharge,R.string.change_data,R.string.change_password,
            R.string.use_explain,R.string.cancel_bind,R.string.install_sure};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerData();
        initView();
        ToptabUtils.titleOnlyToptab(topLeftMenu, back);
        ToptabUtils.setTitle(topTabTitle,"主界面");
        clickEvent();
    }

    private void initRecyclerData(){
        list = new ArrayList<MainPanelModel>();
        MainPanelModel model = null;
        for (int i = 0;i < 9;i++){
            model = new MainPanelModel();
            model.setResId(imgResidArray[i]);
            model.setTitle(getResources().getString(textResidArray[i]));
            list.add(model);
        }


    }

    private void setHeadIconBitmap(){
        String url = BaseApplication.getUserInstance().getInfo().getThumb();
        mCache = DiskLruCacheUtils.getDiskLruCache(BaseApplication.getInstance());
        Bitmap is = DiskLruCacheUtils.getBitmapStreamFromDiscLruCache(mCache,url);
        Log.v("IMG","--"+is);
        if (is == null){
            new OkHttpRequest.Builder()
                    .url(url)
                    .circleImageView(headIcon)
                    .displayImage(new ResultCallback<Object>() {
                        @Override
                        public void onError(Request request, Exception e) {
                            Log.v("IMG",e.getMessage());
                        }

                        @Override
                        public void onResponse(Object response) {
                        }
                    });
        }else {

            Log.v("IMG","cache");
            headIcon.setImageBitmap(is);
        }
    }

    private void initView(){
        topLeftMenu = (ImageView) findViewById(R.id.toptab_left_menu);
        back = (TextView) findViewById(R.id.toptab_back);
        topTabTitle = (TextView) findViewById(R.id.toptab_title);


        mGridView = (GridView) findViewById(R.id.main_panel);
        mGridView.setAdapter(new GridViewAdapter(this,list));

        headIcon = (CircleImageView) findViewById(R.id.head_icon);
        setHeadIconBitmap();

        nickNameTextView = (TextView) findViewById(R.id.nick_name);
        nickNameTextView.setText(BaseApplication.getUserInstance().getInfo().getRealname());
    }

    private void clickEvent(){

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 2){
                    startActivity(new Intent(MainActivity.this,WeatherActivity.class));
                }
                Toast.makeText(MainActivity.this,list.get(position).getTitle()+"-"+position,Toast.LENGTH_SHORT).show();
            }
        });

    }





}
