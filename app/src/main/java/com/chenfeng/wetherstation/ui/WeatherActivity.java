package com.chenfeng.wetherstation.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;

import com.chenfeng.wetherstation.R;
import com.chenfeng.wetherstation.adapter.MeSettingAdapter;
import com.chenfeng.wetherstation.utils.ConstantsUtils;

public class WeatherActivity extends AppCompatActivity {
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        initView();
    }

    private void initView(){
        mWebView = (WebView) findViewById(R.id.id_wheather);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportZoom(false);


        mWebView.setWebViewClient(new MyAppWebViewClient());
        mWebView.loadUrl(ConstantsUtils.WHEATEHRE_TEST_URL);
    }

    /**
     * 解决刷新就使用浏览器打开页面的bug
     */
    public class MyAppWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

//            if(Uri.parse(url).getHost().equals(ConstantsUtils.HOST)) {
//                return false;
//            }
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            view.getContext().startActivity(intent);
            return false;
        }
    }
}
