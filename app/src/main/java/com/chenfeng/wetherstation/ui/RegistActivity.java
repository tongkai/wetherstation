package com.chenfeng.wetherstation.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chenfeng.wetherstation.R;
import com.chenfeng.wetherstation.model.City;
import com.chenfeng.wetherstation.model.District;
import com.chenfeng.wetherstation.model.Province;
import com.chenfeng.wetherstation.utils.ConstantsUtils;
import com.chenfeng.wetherstation.utils.SpinnerDataUtils;
import com.chenfeng.wetherstation.utils.ToptabUtils;
import com.chenfeng.wetherstation.utils.okhttputils.callback.ResultCallback;
import com.chenfeng.wetherstation.utils.okhttputils.request.OkHttpRequest;
import com.chenfeng.wetherstation.widget.TKPasswordEdittext;
import com.squareup.okhttp.Request;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class RegistActivity extends AppCompatActivity {

    private static final int ACCOUNT_EMPTY = 1;
    private static final int PASSWORD_EMPTY = 2;
    private static final int VERIFYCODE_EMPTY = 3;
    private static final int COMPLETE_ADDRESS_EMPTY = 4;
    private static final int AGREE_UNCHECKED = 5;
    private static final int ALL_VALIDATE = 6;

    private ImageView topLeftMenu;
    private TextView back;
    private TextView topTabTitle;

    private EditText edtAccount;
    private TKPasswordEdittext edtPassword;
    private EditText edtCode;//输入验证码
    private EditText edtCompleteAddress;

    private Button btnGetVerifyCode;//获取验证码
    private Button btnRegistNow;//立即注册

    private TextView tvBackLogin;

    private CheckBox cbAgree;

    private Spinner spProvince;
    private Spinner spCity;
    private Spinner spDistrict;

    private List<Province>provinces;
    private Province mProvince;

    private ArrayAdapter<Province> provinceAdapter;
    private ArrayAdapter<City> cityAdapter ;
    private ArrayAdapter<District> districtAdapter ;

    private String phoneNumber;//本机号码


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);


        initSpinnerAdapter();
        initView();
        ToptabUtils.setTitle(topTabTitle,R.string.str_regist);
        ToptabUtils.titleOnlyToptab(topLeftMenu, back);
        clickEvent();
        checkEvent();
        selectEvent();
    }

    private void initView(){
        topLeftMenu = (ImageView) findViewById(R.id.toptab_left_menu);
        back = (TextView) findViewById(R.id.toptab_back);
        topTabTitle = (TextView) findViewById(R.id.toptab_title);

        edtAccount = (EditText) findViewById(R.id.regist_edt_account);
        edtPassword = (TKPasswordEdittext) findViewById(R.id.regist_edt_password);
        edtCode = (EditText) findViewById(R.id.regist_edt_code);
        edtCompleteAddress = (EditText) findViewById(R.id.regist_edt_complete_address);

        btnGetVerifyCode = (Button) findViewById(R.id.btn_get_code);
        btnRegistNow = (Button) findViewById(R.id.btn_regist_immediately);

        tvBackLogin = (TextView) findViewById(R.id.back_login);

        cbAgree = (CheckBox) findViewById(R.id.cb_agree);

        spProvince = (Spinner) findViewById(R.id.sp_province);
        spCity = (Spinner) findViewById(R.id.sp_city);
        spDistrict = (Spinner) findViewById(R.id.sp_district);
        spProvince.setAdapter(provinceAdapter);
    }

    private void initSpinnerAdapter(){

        InputStream in = getResources().openRawResource(R.raw.cities);
        try {
            provinces = SpinnerDataUtils.initData(in);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        provinceAdapter = new ArrayAdapter<Province>(this,R.layout.address_spinner_layout,provinces);
        cityAdapter = new ArrayAdapter<City>(this,R.layout.address_spinner_layout,provinces.get(0).getCities());
        districtAdapter = new ArrayAdapter<District>(this,R.layout.address_spinner_layout,provinces.get(0).getCities().get(0).getDistricts());

        provinceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void clickEvent(){
        btnGetVerifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validateInput() == ACCOUNT_EMPTY){
                    Toast.makeText(RegistActivity.this,"请输入手机号",Toast.LENGTH_SHORT).show();
                }else {
                    getVerifyCode(edtAccount.getText().toString());
                }

                getVerifyCode("13560534234");

            }
        });

        tvBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(RegistActivity.this,MainActivity.class));
            }
        });

        btnRegistNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInput() != ALL_VALIDATE) {
                    Toast.makeText(RegistActivity.this, "请输入所有信息,并勾选同意协议", Toast.LENGTH_SHORT).show();
                } else {

                }
                regist();
            }
        });
    }

    private void checkEvent() {
        cbAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String str = isChecked ? "选了我" : "没选我";
                Toast.makeText(RegistActivity.this, str, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void selectEvent(){
        spProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(RegistActivity.this, parent.getAdapter().getItem(position).toString(), Toast.LENGTH_SHORT).show();

                mProvince = provinces.get(position);
                cityAdapter = new ArrayAdapter<City>(RegistActivity.this,R.layout.address_spinner_layout,mProvince.getCities());
                cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spCity.setAdapter(cityAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(RegistActivity.this,(String)parent.getAdapter().getItem(position).toString(), Toast.LENGTH_SHORT).show();
                districtAdapter = new ArrayAdapter<District>(RegistActivity.this,R.layout.address_spinner_layout,mProvince.getCities().get(position).getDistricts());
                districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spDistrict.setAdapter(districtAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(RegistActivity.this, (String) parent.getAdapter().getItem(position).toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * 获取验证码
     * @param phone
     */
    private void getVerifyCode(String phone){
        new OkHttpRequest.Builder()
                .url(ConstantsUtils.HTTP_HOST + ConstantsUtils.GET_CODE_URL + phone)
                .addHeader("Host", ConstantsUtils.HOST)
                .addParams(ConstantsUtils.CFSOURCE_KEY,ConstantsUtils.CFSOURCE_VALUE)
                .get(new ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Log.e("TAG", "error" + e.getMessage());
                    }

                    @Override
                    public void onResponse(String response) {
                        Log.e("TAG", ConstantsUtils.HTTP_HOST + ConstantsUtils.GET_CODE_URL + "13560534234");
                        Log.e("TAG", response);
                    }
                });
    }

    private void regist(){
        new OkHttpRequest.Builder()
                .url(ConstantsUtils.HTTP_HOST + ConstantsUtils.REGIST_URL)
                .addHeader("Host", ConstantsUtils.HOST)
                .addParams(ConstantsUtils.CFSOURCE_KEY, ConstantsUtils.CFSOURCE_VALUE)
                .addParams(ConstantsUtils.USERNAME,"13564897632")
                .addParams(ConstantsUtils.REALNAME,"张三")
                .addParams(ConstantsUtils.PASSWORD, "12313")
                .addParams(ConstantsUtils.VERIFYCODE,"2654")
                .addParams("dstrtid[]","1")
                .addParams("dstrtid[]", "4")
                .addParams("dstrtid[]", "9")
                .addParams("extendsaddress", "2076房")
                .post(new ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Log.e("TAG", "－－－－－－error-－－－－－－－");
                        Log.e("TAG", request.toString());
                        Log.e("TAG", e.getMessage());
                        Log.e("TAG", "－－－－－－error-－－－－－－－");
                    }
                    @Override
                    public void onResponse(String response) {
                        Log.e("TAG", "post");

                        Log.e("TAG", ConstantsUtils.decodeUnicode(response));

                    }
                });
    }

    /**
     * 简单的输入验证
     * @return
     */
    private int validateInput(){
        if (TextUtils.isEmpty(edtAccount.getText().toString())){
            return ACCOUNT_EMPTY;
        }else if (TextUtils.isEmpty(edtPassword.getText().toString())){
            return PASSWORD_EMPTY;
        }else if (TextUtils.isEmpty(edtCode.getText().toString())){
            return VERIFYCODE_EMPTY;
        }else if (TextUtils.isEmpty(edtPassword.getText().toString())){
            return COMPLETE_ADDRESS_EMPTY;
        }else if (!cbAgree.isChecked()){
            return AGREE_UNCHECKED;
        }
        return ALL_VALIDATE;
    }
}
