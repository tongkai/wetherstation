package com.chenfeng.wetherstation.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.chenfeng.wetherstation.R;
import com.chenfeng.wetherstation.adapter.MeSettingAdapter;
import com.chenfeng.wetherstation.model.MeSettingItem;

public class MeActivity extends AppCompatActivity {

    private ListView settingListView;
    private MeSettingAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        initView();
        clickEvent();
    }

    private void initView(){
        settingListView = (ListView) findViewById(R.id.lv_me_setting);
        mAdapter = new MeSettingAdapter(this);
        settingListView.setAdapter(mAdapter);
    }

    private void clickEvent(){

        View v = findViewById(android.R.id.content);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MeActivity.this,MainActivity.class));
            }
        });

        settingListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MeSettingItem item = (MeSettingItem) parent.getAdapter().getItem(position);
                Toast.makeText(MeActivity.this,item.getText(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
