package com.chenfeng.wetherstation.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chenfeng.wetherstation.R;
import com.chenfeng.wetherstation.base.BaseApplication;
import com.chenfeng.wetherstation.model.InfoJsonModel;
import com.chenfeng.wetherstation.utils.ConstantsUtils;
import com.chenfeng.wetherstation.utils.ToptabUtils;
import com.chenfeng.wetherstation.utils.okhttputils.callback.ResultCallback;
import com.chenfeng.wetherstation.utils.okhttputils.request.OkHttpRequest;
import com.chenfeng.wetherstation.widget.ProgressWheel;
import com.squareup.okhttp.Request;

public class LoginActivity extends AppCompatActivity {

    private ImageView topLeftMenu;
    private TextView back;

    private ViewGroup inputLayout;
    private ImageView iconPsw;
    private EditText edtPsw;
    private TextView tvPsw;
    private TextView tvForgetPsw;

    private Button loginButton;
    private Button registButton;

    //使用popupwindow来实现弹出progressbar
    private PopupWindow popupWindow;
    private View maskView;//实现背景变暗
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
        ToptabUtils.titleOnlyToptab(topLeftMenu, back);
        setPswEdtWidth();
        clickEvent();

    }

    private void initView(){

        topLeftMenu = (ImageView) findViewById(R.id.toptab_left_menu);
        back = (TextView) findViewById(R.id.toptab_back);

        inputLayout = (ViewGroup) findViewById(R.id.layout_input);
        iconPsw = (ImageView) findViewById(R.id.ic_psw);
        tvPsw = (TextView) findViewById(R.id.tv_psw);
        tvForgetPsw = (TextView) findViewById(R.id.tv_forget_psw);
        edtPsw = (EditText) findViewById(R.id.edt_password);

        loginButton = (Button) findViewById(R.id.btn_login);
        registButton = (Button) findViewById(R.id.btn_regist);

        initPopupWindow();
        maskView = findViewById(R.id.bg_mask);
    }

    private void initPopupWindow(){
        Display display = this.getWindowManager().getDefaultDisplay();
        View view = LayoutInflater.from(this).inflate(R.layout.layout_progresspopup,null,false);
        //单位是像素，好坑！！！
        popupWindow = new PopupWindow(view,display.getWidth() / 5,display.getWidth() / 5);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                maskView.setVisibility(View.GONE);
            }
        });
    }

    private void showPopupWindow(){
        View parent = findViewById(R.id.main_content);
        popupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0);
        maskView.setVisibility(View.VISIBLE);
    }

    private void hidePopupWindow(){
        popupWindow.dismiss();
    }

    private void clickEvent(){

        topLeftMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "-", Toast.LENGTH_SHORT).show();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "back", Toast.LENGTH_SHORT).show();
            }
        });

        tvForgetPsw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(LoginActivity.this, "forget", Toast.LENGTH_SHORT).show();

            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showPopupWindow();
                login();
//                startActivity(new Intent(LoginActivity.this, MainActivity.class));

            }
        });

        registButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(LoginActivity.this, RegistActivity.class));
            }
        });

    }

    private void login(){
        new OkHttpRequest.Builder()
                .url(ConstantsUtils.HTTP_HOST + ConstantsUtils.LOGIN_URL)
                .addHeader("Host",ConstantsUtils.HOST)
                .addParams(ConstantsUtils.CFSOURCE_KEY, ConstantsUtils.CFSOURCE_VALUE)
                .addParams(ConstantsUtils.USERNAME, "15914271327")
                .addParams(ConstantsUtils.PASSWORD,"123")
                .post(new ResultCallback<InfoJsonModel>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Log.e("TAG", "error-" + e.getMessage());
                        hidePopupWindow();
                        Toast.makeText(LoginActivity.this,"timeout",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(InfoJsonModel response) {
                        Log.e("TAG", "-－－－－－");
//                        Log.e("TAG", ConstantsUtils.decodeUnicode(response));
                        Log.e("TAG", response.toString());
                        Log.e("TAG", "-－－－－－");

                        if (response.getStatus() == 1){
                            hidePopupWindow();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            //这儿可能会内存泄漏
                            BaseApplication.getUserInstance().setInfo(response.getInfo());
                        }else {
                            
                        }


                    }
                });
    }

    /**
     * 解决密码输入框在布局文件不能精准适配多屏幕的问题
     */
    private void setPswEdtWidth(){

        ViewTreeObserver vto = inputLayout.getViewTreeObserver();

        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                inputLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                ViewGroup.MarginLayoutParams iconPswlp = (ViewGroup.MarginLayoutParams)iconPsw.getLayoutParams();
                int iconPswMargin = iconPswlp.leftMargin + iconPswlp.rightMargin;

                ViewGroup.MarginLayoutParams tvForgetPswlp = (ViewGroup.MarginLayoutParams)tvForgetPsw.getLayoutParams();
                int tvForgetPswMargin = iconPswlp.leftMargin + iconPswlp.rightMargin;

                int edtPswWidth =  inputLayout.getWidth() - iconPsw.getWidth() - tvPsw.getWidth()
                        - tvForgetPsw.getWidth() - iconPswMargin - tvForgetPswMargin;

                ViewGroup.LayoutParams lp = edtPsw.getLayoutParams();
                lp.width = edtPswWidth;
                edtPsw.setLayoutParams(lp);
                edtPsw.invalidate();

            }
        });

    }


}
