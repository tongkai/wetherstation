package com.chenfeng.wetherstation.utils;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 用来管理头部工具条的工具类
 * Created by GEC-IOS100 on 15/11/21.
 */
public class ToptabUtils {

    public static void titleOnlyToptab(ImageView topLeftMenu, TextView back){
        topLeftMenu.setVisibility(View.GONE);
        back.setVisibility(View.GONE);
    }

    public static void setTitle(TextView title,String text){
        title.setText(text);
    }

    public static void setTitle(TextView title,int resid){
        title.setText(resid);
    }

}
