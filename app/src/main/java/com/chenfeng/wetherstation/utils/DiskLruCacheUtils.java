package com.chenfeng.wetherstation.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.chenfeng.wetherstation.libcore.io.DiskLruCache;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 用于图片硬盘缓存的工具类
 * Created by GEC-IOS100 on 15/12/5.
 */
public class DiskLruCacheUtils {

    public static DiskLruCache getDiskLruCache(Context context){
        File cacheDir = getDiskCacheDir(context,"thumb");
        if (!cacheDir.exists()){
            cacheDir.mkdir();
        }
        try {
            return DiskLruCache.open(cacheDir,getAppVersion(context),1,10 * 1024 * 1024);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 将缓存记录同步到journal文件中。
     */
    public static void fluchCache(DiskLruCache mDiskLruCache) {
        if (mDiskLruCache != null) {
            try {
                mDiskLruCache.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeToDiscLruCache(DiskLruCache mDiskLruCache,String url,InputStream inputStream){

        final String key = ConstantsUtils.hashMD5(url);
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        try {
            DiskLruCache.Editor editor = mDiskLruCache.edit(key);
            if (editor != null){
                OutputStream outputStream = editor.newOutputStream(0);
                in = new BufferedInputStream(inputStream,4 * 1024);
                out = new BufferedOutputStream(outputStream,4 * 1024);
                int b;
                while ((b = in.read()) != -1){
                    out.write(b);
                }
                editor.commit();
            }

        } catch (IOException e) {

            e.printStackTrace();
        }finally {

            try{
                if (in != null){
                    in.close();
                }
                if (out != null){
                    out.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }


    }

    public static Bitmap getBitmapStreamFromDiscLruCache(DiskLruCache mDiskLruCache,String url){
        InputStream inputStream = null;
        FileInputStream fileInputStream = null;
        FileDescriptor fd = null;
        DiskLruCache.Snapshot snapshot = null;
        Bitmap bmp = null;
        final String key = ConstantsUtils.hashMD5(url);
        try {
            snapshot = mDiskLruCache.get(key);
            if (snapshot == null){
                return null;
            }
            fileInputStream = (FileInputStream)snapshot.getInputStream(0);
            fd = fileInputStream.getFD();
            if (fd != null){
                bmp = BitmapFactory.decodeFileDescriptor(fd);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bmp;
    }

    private static File getDiskCacheDir(Context context,String fileName){
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()){
            //获取到 /sdcard/Android/data/<application package>/cache
            cachePath = context.getExternalCacheDir().getPath();
        }else {
            //获取到/data/data/<application package>/cache
            cachePath = context.getCacheDir().getPath();
        }
        return new File(cachePath + File.separator + fileName);
    }

    private static int getAppVersion(Context context)  {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }


}
