package com.chenfeng.wetherstation.utils;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 存放了一些常量和通用工具
 * Created by GEC-IOS100 on 15/11/27.
 */
public class ConstantsUtils {

    //网络的常量
    public static String HTTP_HOST = "http://beta.517517.cc/" ;

    public static final String HOST = "beta.517517.cc" ;

    public static final String CFSOURCE_KEY = "cfsource";

    public static final String CFSOURCE_VALUE = "app";

    public static final String USERNAME = "username";

    public static final String PASSWORD = "password";

    public static final String REALNAME = "realname";

    public static final String VERIFYCODE = "verifyCode";

    public static final String LOGIN_URL = "index.php/User/Public/login";

    public static final String REGIST_URL = "index.php/User/Public/rec";

    public static final String GET_CODE_URL = "index.php/Home/Public/smsRqcode/item/1/type/1/mobile/";

    public static final String WHEATEHRE_URL = "index.php/Weather/Board/newest/";

    public static final String WHEATEHRE_TEST_URL = "http://beta.517517.cc/index.php/Weather/Board/newest?longtitude=113.3265&latitude=21.36524&cfsource=app";

    public static final String LONGTITUDE_KEY = "longtitude";

    public static final String LONGTITUDE_VALUE = "113.3265";

    public static final String LATITUDE_KEY = "latitude";

    public static final String LATITUDE_VALUE = "21.36524";

    public static final long CONNECT_TIME_OUT = 3000;

    /**
     * Unicode转转UTF－8
     * @param theString
     * @return
     */
    public static String decodeUnicode(String theString) {
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);
        for (int x = 0; x < len;) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    // Read the xxxx
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException(
                                        "Malformed   \\uxxxx   encoding.");
                        }

                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';
                    else if (aChar == 'n')
                        aChar = '\n';
                    else if (aChar == 'f')
                        aChar = '\f';
                    outBuffer.append(aChar);
                }
            } else
                outBuffer.append(aChar);
        }
        return outBuffer.toString();
    }

    /**
     * MD5算法加密并返回。 
     */
    public static String hashMD5(String value) {
        String md5Value;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(value.getBytes());
            md5Value = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            md5Value = String.valueOf(value.hashCode());
        }
        return md5Value;
    }
    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

}
