package com.chenfeng.wetherstation.utils.okhttputils.request;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import com.chenfeng.wetherstation.base.BaseApplication;
import com.chenfeng.wetherstation.libcore.io.DiskLruCache;
import com.chenfeng.wetherstation.utils.DiskLruCacheUtils;
import com.chenfeng.wetherstation.utils.ImageUtils;
import com.chenfeng.wetherstation.utils.okhttputils.callback.ResultCallback;
import com.chenfeng.wetherstation.widget.CircleImageView;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * Created by zhy on 15/11/6.
 */
public class OkHttpDisplayImgRequest extends OkHttpGetRequest
{
    private ImageView imageview;
    private CircleImageView circleImageView;
    private int errorResId;

    private DiskLruCache mDiskLruCache;

    protected OkHttpDisplayImgRequest(
            String url, Object tag, Map<String,
            String> params, Map<String, String> headers,
            ImageView imageView,CircleImageView circleImageView,int errorResId)
    {
        super(url, tag, params, headers);
        this.imageview = imageView;
        this.circleImageView = circleImageView;
        this.errorResId = errorResId;
        mDiskLruCache = DiskLruCacheUtils.getDiskLruCache(BaseApplication.getInstance());
    }

    private void setErrorResId()
    {
        mOkHttpClientManager.getDelivery().post(new Runnable() {
            @Override
            public void run() {
                imageview.setImageResource(errorResId);
            }
        });
    }

    @Override
    public void invokeAsyn(final ResultCallback callback)
    {
        prepareInvoked(callback);

        final Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(final Request request, final IOException e)
            {
                setErrorResId();
                mOkHttpClientManager.
                        sendFailResultCallback(request, e, callback);

            }

            @Override
            public void onResponse(Response response)
            {
                InputStream is = null;
                try
                {
                    is = response.body().byteStream();
                    /*
                        纪录
                     */
                    DiskLruCacheUtils.writeToDiscLruCache(mDiskLruCache,url,is);
                    DiskLruCacheUtils.fluchCache(mDiskLruCache);

                    ImageUtils.ImageSize actualImageSize = ImageUtils.getImageSize(is);
                    final ImageUtils.ImageSize imageViewSize = ImageUtils.getImageViewSize(imageview);
                    int inSampleSize = ImageUtils.calculateInSampleSize(actualImageSize, imageViewSize);
                    try
                    {
                        is.reset();
                    } catch (IOException e)
                    {
                        response = getInputStream();
                        is = response.body().byteStream();
                    }

                    BitmapFactory.Options ops = new BitmapFactory.Options();
                    ops.inJustDecodeBounds = false;
                    ops.inSampleSize = inSampleSize;
                    final Bitmap bm = BitmapFactory.decodeStream(is, null, ops);


                    mOkHttpClientManager.getDelivery().post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (imageview != null){
                                imageview.setImageBitmap(bm);
                            }
                            if (circleImageView != null){
                                circleImageView.setImageBitmap(bm);
                            }

                        }
                    });
                    mOkHttpClientManager.
                            sendSuccessResultCallback(request, callback);
                } catch (Exception e)
                {
                    setErrorResId();
                    mOkHttpClientManager.
                            sendFailResultCallback(request, e, callback);

                } finally
                {
                    try
                    {
                        if (is != null)
                        {
                            is.close();
                        }
                    } catch (IOException e)
                    {
                    }
                }
            }
        });


    }

    private Response getInputStream() throws IOException
    {
        Call call = mOkHttpClient.newCall(request);
        return call.execute();
    }

    @Override
    public <T> T invoke(Class<T> clazz) throws IOException
    {
        return null;
    }
}
